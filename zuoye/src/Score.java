package UML;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.omg.PortableInterceptor.Interceptor;

public class Score {
    //定义所需的静态变量
    private static Document doc;
    private static double before=0;
    private static double base=0;
    private static double test=0;
    private static double program=0;
    private static double add=0;
    private static double sum=0;
    public static void main(String[] args) throws IOException {
        // TODO Auto-generated method stub
        //配置文件
        Properties properties=new Properties();
        InputStream src = new BufferedInputStream (new FileInputStream("C:\\Users\\Administrator\\Desktop\\total.properties"));
        properties.load(src);
        Enumeration fname=properties.propertyNames();

        //获取总经验值
        double sum_brfore=Integer.parseInt(properties.getProperty("before")) ;
        double sum_base=Integer.parseInt(properties.getProperty("base")) ;
        double sum_test=Integer.parseInt(properties.getProperty("test")) ;
        double sum_program=Integer.parseInt(properties.getProperty("program")) ;
        double sum_add=Integer.parseInt(properties.getProperty("add")) ;

        //读取small.html和all.html文件
        File file1=new File("D:\\作业1\\small.html\\");
        File file2=new File("D:\\作业1\\all.html\\");

        Interceptor(file1,file2,sum_brfore,sum_base,sum_test,sum_program,sum_add);
    }
    private static void Interceptor(File small_files, File all_files, double sum_brfore, double sum_base, double sum_test,
                                    double sum_program, double sum_add) {
        // TODO Auto-generated method stub

        //我所获得的经验值
        double my_before=0;//课前自测部分
        double my_base=0;//课堂完成部分
        double my_test=0;//课堂小测部分
        double my_program=0;//编程题
        double my_add=0;//附加题
        try {
            //转化为Document
            org.jsoup.nodes.Document doc1=Jsoup.parse(small_files,"utf-8");
            org.jsoup.nodes.Document doc2=Jsoup.parse(all_files,"utf-8");

            //对small.html的相对应的经验累积
            if(doc1!=null){
                Elements elements1=doc1.getElementsByClass("interaction-row");
                //经验值
                double jy;
                for(int i=0;i<elements1.size();i++) {
                    if (elements1.get(i).child(1).child(2).toString().contains("已参与&nbsp")) {
                        if (elements1.get(i).child(1).child(0).toString().contains("课堂完成")) {
                            Scanner scanner=new Scanner(elements1.get(i).child(1).child(2).children().get(0).children().get(7).text());
                            jy=scanner.nextDouble();
                            my_base=jy+my_base;
                        }else if(elements1.get(i).child(1).child(0).toString().contains("课堂小测")) {
                            Scanner scanner=new Scanner(elements1.get(i).child(1).child(2).children().get(0).children().get(7).text());
                            jy=scanner.nextDouble();
                            my_test=jy+my_test;
                        }else if (elements1.get(i).child(1).child(0).toString().contains("编程题")) {
                            Scanner scanner=new Scanner(elements1.get(i).child(1).child(2).children().get(0).children().get(7).text());
                            jy=scanner.nextDouble();
                            my_program=jy+my_program;
                        }else if (elements1.get(i).child(1).child(0).toString().contains("附加题")) {
                            Scanner scanner=new Scanner(elements1.get(i).child(1).child(2).children().get(0).children().get(7).text());
                            jy=scanner.nextDouble();
                            my_add=jy+my_add;
                        }
                    } else {
                        if (elements1.get(i).child(1).child(0).toString().contains("课前自测")) {
                            Scanner scanner=new Scanner(elements1.get(i).child(1).child(2).children().get(0).children().get(10).text());
                            jy=scanner.nextDouble();
                            my_before=jy+my_before;
                        }
                    }
                }
            }

            //对all.html的相对应的经验累积
            if(doc2!=null) {
                Elements elements2=doc2.getElementsByClass("interaction-row");
                //经验值
                double jy1;
                for(int i=0;i<elements2.size();i++) {
                    if (elements2.get(i).child(1).child(0).toString().contains("课前自测")) {
                        Scanner scanner2=new Scanner(elements2.get(i).child(1).child(2).children().get(0).children().get(10).text());
                        jy1=scanner2.nextDouble();
                        my_before=jy1+my_before;
                    }
                }

            }
            //计算所得分
            before=my_before/sum_brfore*100*0.25;
            base=my_base/sum_base*100*0.95*0.3;
            test=my_test/sum_test*100*0.2;
            program=my_program/sum_program*100*0.1;
            add=my_add/sum_add*100*0.05;
            sum=(base+before+test+program+add)*0.9+5;
            //打印输出
            System.out.println("before:"+before);
            System.out.println("base:"+base);
            System.out.println("test:"+test);
            System.out.println("program:"+program);
            System.out.println("add:"+add);
            System.out.println(String.format("%.2f",sum));
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }
}